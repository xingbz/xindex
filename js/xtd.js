cache_key = "xtd_tasks";

/*
 [
 {
                    "name": "未完成",
                    "status": false
                },
 {
                    "name": "已完成",
                    "status": true
                }
 ]
 */
new Vue({
    el: '#taskList',
    data: function () {
        let cache_value_str = localStorage.getItem(cache_key);
        let tasks = cache_value_str ? JSON.parse(cache_value_str) : [];
        return {
            title: 'to do list',
            tasks: tasks
        };
    },
    methods: {
        newItem: function () {
            if (!this.tasks.name) {
                return
            }
            this.tasks.push({
                name: this.tasks.name,
                status: false
            });
            localStorage.setItem(cache_key, JSON.stringify(this.tasks));
            this.tasks.name = "";
        },
        changeItem: function (task) {
            task.status = !task.status;
            localStorage.setItem(cache_key, JSON.stringify(this.tasks));
        },
        delItem: function (task) {
            this.tasks.splice(this.tasks.indexOf(task), 1);
            localStorage.setItem(cache_key, JSON.stringify(this.tasks));
        }
    }
})